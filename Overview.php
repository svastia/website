<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        <!--
        <div class="cb-slideshow">
            <header id="landing-page">
                <div class="container">
                    <div class="page-title shadow">
                        <div class="">Welcome To Cambridgene!</div>
                    </div>
                </div>
            </header>
        </div>
        -->
        <section class="bb nbb c-section">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 para-norm">
                    <h2 class="section-sub-heading nbb">About us</h2>
                
<p>Our mission is to improve people’s lives through precision medicines.</p>

<p>Cambridgene unlocks the power of multi-omics to accelerate and de-risk pharmaceutical research and development. Our advanced research workflows and genetic profiling solutions accelerate the development and delivery of precision medicines. We have experience gained from developing solutions for analyzing thousands of patients in academic research institutions. 
</p>
<p>Cambridgene enables pharmaceutical companies to deploy the highest quality genomic analysis combined with the best data representation to support decision-making and stratification of results.
</p>
<p>Personalized medicine is a revolution capable of transforming people’s lives, however, challenges from data quality, lack of infrastructure for automation of analysis, to the complexity interpreting results, restrict faster uptake. Cambridgene has built the infrastructure to offer robust large-scale analysis needed for reliable deployment in clinical research projects. 
</p>
<p>We work closely with pharmaceutical companies and large research organisations to deliver cutting-edge, transferable research tools that accelerate clinical trials and de-risk research and development.
</p>
<p>We believe that the stratification of data using ‘omics strategies offers significant commercial benefits that will transform pharmaceutical development and enable stratified medicines.
</p>
<p>Speak to us to find out how our tools can de-risk your preclinical and clinical development.
</p>

            </div>
        </section>
        

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/vendor/jquery.validate.min.js"></script>
        <script src="assets/js/vendor/additional-methods.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

        
    </body>
</html>

<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        <!--
        <div class="cb-slideshow">
            <header id="landing-page">
                <div class="container">
                    <div class="page-title shadow">
                        <div class="">Welcome To Cambridgene!</div>
                    </div>
                </div>
            </header>
        </div>
    -->
        <section class="bb nbb c-section">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 para-norm">
                    <h2 class="section-sub-heading nbb">Data Management</h2>

<p>Cambridgene’s team has routinely deployed secure data management in multiple clinical diagnosis projects. </p>

<h2 class="section-sub-heading-2 nbb">Data Protection / Confidentiality / Security</h2>

<p>Data protection, security and confidentiality are critical to our business. Our data management and analysis platforms use 1SO 27001/2 compliant security architecture, access control and availability with auditability and compliant assessment provisions.</p>

<p>Upon request, we review customers’ data compliance expectations for handling PHI (Protected Health Information) – as defined in HIPAA and/or other similar mechanisms.</p>

<h2 class="section-sub-heading-2 nbb">Internal Deployment</h2>

<p>Cambridgene has also built tools that are transferable to customers’ internal network premises. We deliver applications that are easy to deploy, manage and customizable for our customers’ internal infrastructure.</p> 

<p>Applications, databases (public domain and private), repositories can be deployed and accessed fully locally.</p>
        
<h2 class="section-sub-heading-2 nbb">UK / European Data Management</h2>

<p>If you are looking for data management and analysis solutions from a state-certified UK / European provider, please get in touch with us to discuss your requirements. We also offer government-certified, cloud-hosted solutions for your institution (e.g., NHS) depending on your requirements.</p>

	</div>
            </div>
        </section>
        

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/vendor/jquery.validate.min.js"></script>
        <script src="assets/js/vendor/additional-methods.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

        
    </body>
</html>

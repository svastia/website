<!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll custom-logo" href="index.php"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <!--
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php">Home</a>
                    </li>

                   
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Solutions<span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="Biomarker-Stratification.php">Biomarker Stratification</a></li>
                        <li class="divider"></li>
                        <li><a href="Accelerate-Antibody-RD.php">Accelerating Antibody R & D</a></li>
                        <li class="divider"></li>
                        <li><a href="Health-Demographics.php">Health Demographics</a></li>
                      </ul>
                    </li>

                     <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Expertise<span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="Data-Integration-QC.php">Data Integration & QC</a></li>
                        <li class="divider"></li>
                        <li><a href="Genomic-Variation.php">Genomic Variation Analysis</a></li>
                        <li class="divider"></li>
                        <li><a href="Biomarker-Analysis.php">Biomarker Analysis</a></li>
                        <li class="divider"></li>
                        <li><a href="Data-Access.php">Public-domain Data Access</a></li>
                        <li class="divider"></li>
                        <li><a href="Workflows.php">Advanced Workflows</a></li>
                      </ul>
                    </li>

                     <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About<span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="Overview.php">Overview</a></li>
                        <li class="divider"></li>
                        <li><a href="Team.php">Team</a></li>
                        <li class="divider"></li>
                        <li><a href="How-We-Work.php">How we work</a></li>
                        <li class="divider"></li>
                        <li><a href="Data-Management.php">Data Management</a></li>
                        <li class="divider"></li>
                        <li><a href="Ethics.php">Ethics</a></li>
                        <li class="divider"></li>
                        <li><a href="Social-Impact.php">Social Impact</a></li>
                      </ul>
                    </li>
                    
                    <li>
                        <a href="Contact.php">Contact</a>
                    </li>
                </ul>
            </div>
            -->
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

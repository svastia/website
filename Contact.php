<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        
        <div class="cb-slideshow">
            <header id="contact">
                <div class="container">
                    <div class="page-title shadow">
                        
                    </div>
                </div>
            </header>
        </div>
        <div>&nbsp;</div>
        <section class="nbp bb">
            <div class="container">
             <div class="col-md-8 col-sm-offset-2">
             
                    <h2 class="section-sub-heading nbb">Contact Us</h2>
                    <h2 class="section-sub-heading-2 nbb">Address</h2>

                <div class="col-md-12">
                    <p>Future Business Centre, Kings Hedges Road, Cambridge CB4 2HY</p>
                    <p>Stevenage Bioscience Catalyst, Gunnels Wood Road, Stevenage SG1 2FX</p>
                    <p>0845 4562432, 07853 198595</p>
                    <p>&#032;&#105;&#110;&#102;&#111;&#064;&#099;&#097;&#109;&#098;&#114;&#105;&#100;&#103;&#101;&#110;&#101;&#046;&#099;&#111;&#109;</p>
                </div>
                <br>
                <div>&nbsp;</div>
                <h2 class="section-sub-heading-2 nbb">Location</h2>
                <p>Our office at the Future Business Centre is located close to the Cambridge Science Park. </p>
                <p>Our office is also close to other leading institutions: </p>
                <ul>
                    <li>The University of Cambridge</li>
                    <li>Addenbrooke’s Hospital</li>
                    <li>The Biomedical Research Campus (MRC Laboratory of Molecular Biology, AstraZeneca, etc.)</li>
                    <li>The Babraham Research Campus</li>
                    <li>Granta Park</li>
                    <li>Chesterford Research Park</li>
                    <li>Wellcome Trust Sanger Institute / European Bioinformatics Institute</li>
                </ul>
                <p>We also have a part-time office at the Stevenage Bioscience Catalyst to meet our customers. </p>
                <br>
                <br>
            </div>
        </div>
        </section>  

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/vendor/jquery.validate.min.js"></script>
        <script src="assets/js/vendor/additional-methods.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

       
    </body>
</html>

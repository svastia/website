<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        <!--
        <div class="cb-slideshow">
            <header id="landing-page">
                <div class="container">
                    <div class="page-title shadow">
                        <div class="">Welcome To Cambridgene!</div>
                    </div>
                </div>
            </header>
        </div>
    -->
        <section class="bb nbb c-section">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 para-norm">
                    <h2 class="section-sub-heading nbb">Advanced Workflows</h2>

<p>If your analysis requires partly or fully automated workflows for speeding up the analysis, we offer internal or secure, cloud-hosted solutions.
</p>

<p>
We offer workflows various analyses:
</p>

<ul>
<li>Antibody analysis – sequence processing, NGS analysis, annotation, etc.</li>
<li>Genomic variation analysis – CNVs, SNVs/INDELs, biomarker relationship mapping, etc.</li>
<li>Data quality control – contamination, sample and data QC metrics, filters, etc.</li>
<li>Data integration – clinical research, clinical trials, public-domain with internal data, etc.</li>
<li>Customized workflows – internal and private cloud deployment</li>
</ul>

<p>
All workflows include bespoke data protection/security as explained in our Data Management section. Please get in touch with us to discuss your requirements.
</p>

                </div>
            </div>
        </section>
        

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/vendor/jquery.validate.min.js"></script>
        <script src="assets/js/vendor/additional-methods.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

       
    </body>
</html>

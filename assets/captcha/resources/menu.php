<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<title>The Staiths Cafe</title>

<link rel="stylesheet" href="media/css/font-awesome.min.css" />
<link rel="stylesheet" href="media/css/foundation.min.css" />
<link href='http://fonts.googleapis.com/css?family=Varela+Round'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Raleway:300,500|Arvo:700'
	rel='stylesheet' type='text/css'>

<link
	href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700|Open+Sans:400,300,300italic,400italic,600,600italic,700,800,700italic,800italic'
	rel='stylesheet' type='text/css'>

<link
	href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Fjalla+One'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"
	href="media/css/jquery.datetimepicker.css">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="media/js/foundation/vendor/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="media/css/menu-style.css" />
<link rel="stylesheet" href="media/css/style.css" />
<style type="text/css">
.staiths-footer-top{
	border-top: 1px solid #dddde3;
	background-color: #f1f1f4;
	font-size: 0.9em;
}
</style>
</head>
<body>
	<div id="main-wrapper">
		<div class="row" id="content-wrapper">
			<?php include_once 'header.php'; ?>
			<div class="hide-for-small" style="margin-top: 5px;" ></div>
			<div id="menu">
				<div>&nbsp;</div>
				<section class="main">

					<div id="rm-container" class="rm-container">

						<div class="rm-wrapper">

							<div class="rm-cover">

								<div class="rm-front">
									<div class="rm-content">
										<div>&nbsp;</div>
										<div class="rm-logo"></div>
										<h2>&nbsp</h2>
										<h3>Your cafe, your coffee, your space, your view, your side
											of the Tyne.</h3>

										<a href="#" class="rm-button-open">View the Menu</a>
										<div class="rm-info">
											<p>
												1 Autumn Drive<br> Staiths Southbank<br> Gateshead<br> NE8
												2BZ<br> <strong><hr><br></strong> 07733 335313<br> <strong></strong>
												07900 147628<br>staithscafebar@gmail.com<br><a href="https://www.facebook.com/staithscafe"><img
									class="icon_" alt="" src="media/img/facebook.png"></a> <a
									href="http://instagram.com/staithscafebar"><img class="icon_"
									alt="" src="media/img/instagram.png"></a> <a
									href="https://twitter.com/staithscafebar"><img class="icon_"
									alt="" src="media/img/twitter.png"></a><a href="https://www.pinterest.com/staithscafebar"><img
					class="icon_" alt="" src="media/img/pinterest.png"></a> 
											</p>
										</div>

									</div>
									<!-- /rm-content -->
								</div>
								<!-- /rm-front -->

								<div class="rm-back">
									<div class="rm-content">
										<h4>Breakfast</h4>
										<dl>
											<dt>Staiths Full English</dt>
											<dd>2 bacon, 2 sausage, 2 slices of black pudding, beans, fresh tomato, egg and brown or white toast</dd>
											<dt>Breakfast Roll</dt>
											<dd>Any items above</dd>
											<dt>Omelette (Plain or Filling)</dt>
											<dd>Ham, cheese, mushroom, bacon and tomato fillings</dd>
											<dt>Egg Bread</dt>
											<dd>Made with plain or cheesy sourdough</dd>
	
										<dt>Free Range Omelette</dt>
											<dd>Add garnish, mushrooms, cheese, ham etc</dd>
										<dt>Granola or museli </dt>
										     <dd> with fruit and yoghurt </dd>
										<dt>Porridge sweetened </dt>
										     <dd> with Chainbridge comb honey</dd>
										</dl>
										<dd> Take away and pre order available </dd>
										<dd> Delivery within a mile radius is coming soon</dd>
										<dd> For more details call us 07733 335313 </dd>

																			</div>
									<!-- /rm-content -->
									<div class="rm-overlay"></div>

								</div>
								<!-- /rm-back -->

							</div>
							<!-- /rm-cover -->

							<div class="rm-middle">
								<div class="rm-inner">
									<div class="rm-content">
									<h4>Sandwiches and Paninis or Ciabatta</h4>
										<dl>
										<dd>Smoked Ham, Applewood cheese and caramelised Onion</dd>
                                        <dd>Mozzarella, fresh basil and sundried tomato (V)</dd>
											
										</dl>
										<h4>Sourdough Sarnies</h4>
										<dl>
											<dt>Chainbridge honey mustard and ham</dt>
										</dl>
										<h4>Soups & Salads</h4>
										<dl>
											<dd>Fresh homemade soup</dd>
											<dd> with chunk of Sourdough </dd>
											<dd>Chicken and bacon</dd>
											<dd>Ham and cheese</dd>
											<dd>Mozzerella and 2 type tomatoes</dd>
											<dd>Chickpea and hummous</dd>
										</dl>
																			
										<dd> All our sourdoughs, paninis, buns and loaves are delivered daily by The Original Bakerhouse </dd>
									</div>
									<!-- /rm-content -->
									<div class="rm-overlay"></div>
								</div>
								<!-- /rm-inner -->
							</div>
							<!-- /rm-middle -->

							<div class="rm-right">

								<div class="rm-front"></div>

								<div class="rm-back">
									<span class="rm-close">Close</span>
									<div class="rm-content">
									<dl>
											<h4>Evening Specials</h4>
											<dt>Curry night</dt>
											<dt>Scream for Pizza</dt>
										</dl>
										<h4>Hot and Cold drinks</h4>
										<dl>
											<dd> Cappucino, Americano, Latte, tea and choclate drink.
                                             We also have wide range of juices, smoothies, Milk, energy drinks and coke. 											
										   </dd>
											<dl>
											<h4>Alcholic Drinks & Other</h4>
											<dd> We have wide range of alcholic in bottles including cider, champange and vines
											</dd>
											</dl>
										<dl>
											<h4>Cakes and Treats</h4>
											<dt>
												The Most delicious brownie </dt>											</dt>
											</dl>
											<dt> Notes:</dt>
											<dd> Vegetarian (V) </dd>
											<dd> Gluten Free (GF) </dd>
											<dd> Child Friendly Portions(C) </dd>
											
									</div>
									<!-- /rm-content -->
								</div>
								<!-- /rm-back -->

							</div>
							<!-- /rm-right -->
						</div>
						<!-- /rm-wrapper -->

					</div>
					<!-- /rm-container -->

				</section>
			</div>
			<div class="clearfix">&nbsp;</div>
			<!-- jQuery if needed -->
			<script type="text/javascript"
				src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
			<script type="text/javascript" src="media/js/menu.js"></script>
			<script type="text/javascript">
				$(function() {
					Menu.init();
				});
			</script>
		</div>
		<?php include_once 'footer.php'; ?>
	</div>

	<script src="media/js/vendor/jquery.js"></script>
	<script src="media/js/jquery.datetimepicker.js"></script>

	<script src="media/js/foundation.min.js"></script>



	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
      $(document).foundation({});
    </script>
	<script>
		$(document).foundation({});
		  $(function(){
			    var current = location.pathname.split("/")[2];
			    console.log(current);
			    $('#nav li a').each(function(){
			        var $this = $(this);
			        // if the current path is like this link, make it active
			        if($this.attr('href').indexOf(current) !== -1){
			            $this.addClass('activated');
			        }else{
			        	$this.removeClass('activated');
			        }
			    })
			})
		    </script>
</body>
</html>
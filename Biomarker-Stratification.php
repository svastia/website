<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        <!--
        <div class="cb-slideshow">
            <header id="landing-page">
                <div class="container">
                    <div class="page-title shadow">
                        <div class="">Welcome To Cambridgene!</div>
                    </div>
                </div>
            </header>
        </div>
        -->
        <section class="bb nbb c-section">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 para-norm">
                    <h2 class="section-sub-heading nbb">Biomarker Stratification</h2>
                        <p>
                            Our solutions provide advanced data integration and quality control of clinical, biomarker and multi-omics data to ensure scientific validity. We can provide data integration and dynamic visualization/exploration from multiple sources (e.g., clinical, genomics, proteomics) as well as multiple sites. 
                        </p>
                                            <div class="box">
                        <ul>
                            <li>Ensure highest data quality</li>
                            <li>Explore, visualize novel patterns</li>
                            <li>Integrate multi-domain data with ease</li>
                        </ul>
                    </div>
                        <p>
                            We have extensive experience of multi-omics data quality control in thousands of patients in several academic projects. We collaborate with researchers, hospitals and data providers to implement the latest QC standards.
                            We use a multitude of custom and third party applications that can de deployed either locally within the internal infrastructure, or through secure cloud services. 
                        </p>

                        <p>
                            We have workflows to integrate and QC data from clinical trials, genomic data (whole-genomes, exomes, gene panels), potential biomarker targets and drug molecules (small molecules and biologics) that are available in the public domain. 
                        </p>

                        <p>
                            Our research workflows can uncover unique patterns and potential combinatorial effects based on data integration from multiple domains. 
                        </p>

                </div>
            </div>
        </section>
        

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/vendor/jquery.validate.min.js"></script>
        <script src="assets/js/vendor/additional-methods.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

        
    </body>
</html>

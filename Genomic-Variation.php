<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        <!--
        <div class="cb-slideshow">
            <header id="landing-page">
                <div class="container">
                    <div class="page-title shadow">
                        <div class="">Welcome To Cambridgene!</div>
                    </div>
                </div>
            </header>
        </div>
    -->
        <section class="bb nbb c-section">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 para-norm">
                    <h2 class="section-sub-heading nbb">Genomic Variation Analysis</h2>

<p>Our advanced variant analysis pipelines use the academic or industry standard as the starting point and extend them with advanced features and functionalities.</p>

<p>We follow a process similar to the standard BWA-Picard-GATK workflow, substituting with a few well-tested tools/stages with faster, resource-optimized (RAM, storage), cloud-friendly alternatives. In addition, we also deploy a combined variant calling strategy using multiple callers to maximize the diagnostic rate, reliability and to minimize false detections.</p>

<h2 class="section-sub-heading-2 nbb">Copy Number Analysis</h2>

<p>We specialize in CNV detection from whole-exome, whole-genome and targetted gene panel data. We have handled CNV analysis in several large-scale projects. </p>

<p>Our bespoke workflows offer CNV analysis together with sample QC, pre-, and post-CNV detection QC, filtering and disease biology for various common and rare diseases. We have also handled cancer samples (FFPE and Fresh Frozen tissues) for which we have developed customized QC and analysis.</p>

<h2 class="section-sub-heading-2 nbb">SNVs and INDELs</h2>

<p>We use a combination of tools depending on the end users’ requirements. We make use of this data not only for the analysis of disease biology, but also for the detection of contamination, sample/data quality, etc. </p>

<h2 class="section-sub-heading-2 nbb">Trisomies and Uniparental Disomies</h2>

<p>From the detection of Trisomies and UPDs, we use multiple analysis methods/tools for whole-genome, whole-exome, gene panels and arrays. </p>

<h2 class="section-sub-heading-2 nbb">Other Key Areas</h2>

<ul>
<li>Gene/Protein Interactions (extensive workflows to integrate with genomic data, metabolomics data, biomarker validation as well as drug efficacy and safety profiles).</li>
<li>Disease / phenotype associations (biomarker mutation signatures, known targets available in the public domain, etc.)</li>
<li>Multi-omic data integration: Integrate clinical data (clinical trials, (pre) clinical research data) with genomic, proteomic and metabolomic data</li>
</ul>

                </div>
            </div>
        </section>
        

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/vendor/jquery.validate.min.js"></script>
        <script src="assets/js/vendor/additional-methods.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

        
    </body>
</html>

<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Welcome to Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        
        <div class="cb-slideshow">
            <header id="landing-page">
                <div class="container">
                    <div class="col-md-4">
                        <!--
                        <div class="page-title shadow">
                            <p class="header-content animated slideInRight" href="" >Biomarker Stratification</p>
                            <p class="header-content animated slideInLeft" href="">Accelerate Antibody R&amp;D</p>
                            <p class="header-content animated slideInRight" href="">Health Demographics</p>
                        </div>
                        -->
                    </div>
                </div>
            </header>
        </div>
        <br>
        <!--br-->
        <section class="nbp">
            <div class="container">
              <p class="banner-heading-one"><a href="https://svastia.ai" target="_blank">Svastia&trade;</a><i class="fas fa-link"></i> provides robust multi-omic analytics workflows and access to novel clinical genomic data to accelerate clinical research.</p>               

              <p class="banner-heading-two">Partner with us to conduct clinical studies to advance biomarker discovery and patient stratification strategies. Please <a href="mailto:info@cambridgene.com">contact us</a> <i class="fas fa-link"></i> for more info.</p> 
               
<!--
                <div class="heading text-center">
                    Cambridgene unlocks the power of multi-omics to accelerate and de-risk pharmaceutical research and development. Our advanced clinical research workflows and genetic profiling solutions accelerate the development and delivery of precision medicines.
                    <a name="solutions"></a> 
                </div-->
            </div>
        </section>
        <!--br>
        <br-->
        <!--
        <section class="solutions c-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-4">
                            <div class="box">
                                <p class="text-center"><img src="assets/img/hs1.png" height="100"  ></p>
                                <h4 id="download-bootstrap"><a href="Biomarker-Stratification.php">Biomarker Stratification</a></h4>
                                <p>Identify and validate biomarkers for stratification in (pre)-clinical research and clinical trials – Integrate and QC clinical and multi-omic data from various domains and sources</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box">
                                <p class="text-center"><img src="assets/img/antibody.png" height="100"  ></p>
                                <h4 id="download-bootstrap"><a href="Accelerate-Antibody-RD.php">Accelerate Antibody R&amp;D</a></h4>
                                <p>Accelerate antibody R &amp; D using our customized workflows for QC, screening, selection, annotation and integration with public domain and patented data</p>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box">
                                <p class="text-center"><img src="assets/img/dna.png" height="100"  ></p>
                                <h4 id="download-bootstrap"><a href="Health-Demographics.php">Health Demographics</a></h4>
                                <p>Empower pharmaceutical research through collection and dissemination of accurate, representative data of disease portfolio in South Asia and other geographical regions</p>                               
                            </div>
                        </div>

                    </div>

                </div>
                 <a name="expertise"></a> 
            </div>
        </section>  
         <div class="cb-slideshow">
            <header class="light">
                <section class="c-section"> 
                <div class="container">
                    <h2 class="section-heading">Our Expertise</h2>
                    <div class="text-left col-sm-10 col-sm-offset-1">
                        <div class="col-sm-6">
                            <div class="text-right">
                                <h4 id="download-bootstrap"><a href="Data-Integration-QC.php" >Data Integration and Quality</a></h4>
                                <p>Bespoke data integration and quality control for clinical and multi-omic data</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="">
                                <h4 id="download-bootstrap"><a href="Genomic-Variation.php" >Genomic Variation Analysis</a></h4>
                                <p>Variation detection, QC metrics/filters, validation and comparison with external datasets</p>
                                
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-6">
                            <div class="text-right">
                                <h4 id="download-bootstrap"><a href="Biomarker-Analysis.php" >Biomarker Analysis</a></h4>
                                <p>Identify and validate biomarkers for stratification in (pre)-clinical research and clinical trials</p>
                                
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="">
                                <h4 id="download-bootstrap"><a href="Data-Access.php" >Public Domain Data Access</a></h4>
                                <p>Find the relevant research studies from the public domain and gain access to open and managed access datasets</p>
                                
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="text-center">
                                <h4 id="download-bootstrap"><a href="Workflows.php" >Advanced Workflows</a></h4>
                                <p>Bespoke workflows (locally-deployed and cloud-based) with customization for accelerating your R &amp; D</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
               </section>
              -->
            </header>
        </div>

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>
       <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->


        <script>
    	    (function(i, s, o, g, r, a, m) {
        	i['GoogleAnalyticsObject'] = r;
        	i[r] = i[r] || function() {
           	 (i[r].q = i[r].q || []).push(arguments)
       		 }, i[r].l = 1 * new Date();
        	a = s.createElement(o),
            	m = s.getElementsByTagName(o)[0];
        	a.async = 1;
        	a.src = g;
        	m.parentNode.insertBefore(a, m)
    	     })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-72969251-1', 'auto');
	ga('send', 'pageview');

	</script>

        
    </body>
</html>

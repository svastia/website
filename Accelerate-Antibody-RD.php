<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cambridgene</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php 
            include_once("page-includes.php");
        ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php 
            include_once("header.php");
        ?>
        
        <div class="cb-slideshow">
            <header id="ab-page">
                <div class="container">
                    <div class="page-title shadow">
                        <div class=""></div>
                    </div>
                </div>
            </header>
        </div>
        <section class="bb nbb">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 para-norm">
                    <h2 class="section-sub-heading nbb">Accelerate Antibody R &amp; D</h2>

<p>Cambridgene Antibody Informatics Toolkit (CAIT) is an advanced, customizable product for accelerating antibody research and development, developed together with companies in Cambridge (UK) and London.</p>

<div class="box">
<ul>
<li>Accelerate antibody development and product delivery</li>
<li>Highly customizable to internal use</li>
<li>Bundled consulting for customization and new features</li>
<li>Rapid deployment/update, Flexible number of concurrent users
</ul>
</div>

<p>It consists of ready-to-deploy modules and is offered with bundled consulting for developing and incorporating new features that are customized to accelerate routine antibody research and development workflows. </p>

<h2 class="section-sub-heading-2 nbb">Features</h2>

<ul>
<li>	Informatics for routine analysis – pre-developed and customizable workflows to gain edge over competitors’ product delivery timelines</li>
<li>	Integrate multiple tools for informatics analysis, with inputs/outputs easily shuffling between the tools/steps</li>
<li>	Automated pipelines for QC, screening, selection and annotation of antibodies</li>
<li>	Analysis of disease target relationships, access (secure, internal) to public domain datasets</li>
<li>	Sort antibody sequences using sequenced-based and biochemical features</li>
<li>	Export and import to widely accessible file formats (fasta, Excel, etc.)</li>
<li>	Integration of small and large molecule datasets (internal and external) – visualize and explore the landscape of biological relationships and combinatorial effects</li>
<li>	NGS-integration (internal and cloud-based analysis), with support for vendor-specific NGS file formats</li>
</ul>

                </div>
            </div>
            <br>
            <div>&nbsp;</div>
        </section>
        

        <?php 
            include_once("footer.php");
        ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.js"></script>
        <script src="assets/js/vendor/jquery.validate.min.js"></script>
        <script src="assets/js/vendor/additional-methods.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

        
    </body>
</html>
